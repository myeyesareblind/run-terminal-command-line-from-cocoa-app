//
//  EncryptHandler.h
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EncryptHandler : NSObject
+ (void)encryptFileAtPath:(NSString*)path;
+ (void)decryptFileAtPath:(NSString*)path;
@end

NS_ASSUME_NONNULL_END
