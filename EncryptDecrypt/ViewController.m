//
//  ViewController.m
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import "ViewController.h"
#import "EDDragHandleView.h"
#import "EncryptHandler.h"

@interface ViewController() <EDDragHandleViewHandler>

@property (weak) IBOutlet NSButton *encryptOrDecryptSwitch;
@property (readwrite) EDDragHandleView *view;

@end

@implementation ViewController
@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.dragHandler = self;
}

- (void)dragHandleView:(EDDragHandleView *)aView didRecieveFileAtPath:(NSString *)path {
    NSAssert(path != nil, @"not nil expected");
    if (self.encryptOrDecryptSwitch.state == NSControlStateValueOn) { /// encrypt selected
        [EncryptHandler encryptFileAtPath:path];
    }
    else {
        NSAssert([path.pathExtension isEqualToString:@"enc"], @"to decrypt - use a file with .enc extension");
        [EncryptHandler decryptFileAtPath:path];
    }
    
}

@end
