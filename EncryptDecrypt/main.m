//
//  main.m
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
