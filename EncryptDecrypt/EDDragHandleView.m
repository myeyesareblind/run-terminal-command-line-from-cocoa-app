//
//  EDDragHandleView.m
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import "EDDragHandleView.h"

@implementation EDDragHandleView

- (instancetype)init {
    self = [super init];
    if (self) {
    
    }
    return self;
}

- (void)awakeFromNib {
    [self registerForDraggedTypes:@[NSPasteboardTypeFileURL]];
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    return NSDragOperationCopy;
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)sender {
    NSURL *fileURL = [NSURL URLFromPasteboard:[sender draggingPasteboard]];

    [self.dragHandler dragHandleView:self didRecieveFileAtPath:fileURL.path];
    
    return YES;
}

@end
