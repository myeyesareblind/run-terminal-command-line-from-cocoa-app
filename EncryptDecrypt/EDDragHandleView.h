//
//  EDDragHandleView.h
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class EDDragHandleView;
@protocol EDDragHandleViewHandler
- (void)dragHandleView:(EDDragHandleView*)aView didRecieveFileAtPath:(NSString*)path;
@end

NS_ASSUME_NONNULL_BEGIN

@interface EDDragHandleView : NSView
@property (readwrite, weak) id <EDDragHandleViewHandler> dragHandler;
@end

NS_ASSUME_NONNULL_END
