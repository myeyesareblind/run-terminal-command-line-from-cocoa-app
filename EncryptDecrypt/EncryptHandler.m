//
//  EncryptHandler.m
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import "EncryptHandler.h"

@implementation EncryptHandler

+ (void)encryptFileAtPath:(NSString*)path {
    NSString *command =
    [NSString stringWithFormat:@"openssl enc -aes-256-cbc -salt -in %@ -out %@.enc -pass pass:123456",
     path,
     path];
    system(command.UTF8String);
}

+ (void)decryptFileAtPath:(NSString*)path {
    NSString *command =
    [NSString stringWithFormat:@"openssl enc -aes-256-cbc -d -in %@ -out %@ -pass pass:123456",
     path,
     [path stringByDeletingPathExtension]];
    
    system(command.UTF8String);
}

@end
