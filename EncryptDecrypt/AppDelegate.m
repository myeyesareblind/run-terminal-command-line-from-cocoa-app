//
//  AppDelegate.m
//  EncryptDecrypt
//
//  Created by Maksym Stobetskyi on 10/02/2019.
//  Copyright © 2019 Maksym Stobetkyi. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
